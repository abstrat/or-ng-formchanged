== Installation ==

Install with Bower:
bower install https://marclloyd77@bitbucket.org/marclloyd77/or-ng-formchanged.git


Inject orNgUser as a dependecy in app.js:
angular.module('myApp', [ 'orNgFormChanged' ])


== Usage ==

Add the haschanged directive above your form. The scope attribute if the form object (form name)

<haschanged scope="newUserForm"></haschanged>

We then need to set the form as pristine in the success callback in your controller:

$scope.formName.$setPristine();