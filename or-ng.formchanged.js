'use strict';

/**
 * @ngdoc directive
 * @name or-ng.formchanged
 * @description
 * # orformchanged
 * Usage:  <haschanged scope="newUserForm"></haschanged>
 */

angular.module('orNgFormChanged', [])
    .directive('haschanged', function ( $location, $rootScope ) {
        return {
            restrict: 'E',
            scope: false,
            transclude: true,
            template: '<div class="or-ng-fc-modal-wrap" ng-show="changeAlert"><div class="or-ng-fc-modal">' +
            '<p>This form has not been saved. Do you still want to navigate away?</p>' +
            '<a href="" class="btn align-left" ng-click="leavePage()">Yes, navigate away</a>' +
            '<a href="" class="btn align-right" ng-click="stayHere()">No, stay on this page</a>' +
            '</div></div>',
            link: function postLink(scope, element, attrs) {

                scope.$watch(attrs.scope, function(newValue, oldValue) {

                    scope.$on('$locationChangeStart', function(e, nextLocation) {
                        // Continue if form is not dirty
                        if ( newValue.$pristine || scope.allowChange ){
                            return;
                        }else{
                            // Get next page location
                            scope.nextLocation = nextLocation.split('#');
                            scope.changeAlert = true;
                            e.preventDefault();
                        }
                    });

                    scope.$on('$routeChangeSuccess', function(e, nextLocation) {
                        scope.allowChange = false;
                    });

                }, true);

                scope.stayHere = function(){
                    scope.changeAlert = false;
                };

                scope.leavePage = function(){
                    scope[attrs.scope].$setPristine();
                    scope.allowChange = true;
                    $location.path( scope.nextLocation[1] );
                }

            }
        };
    });

